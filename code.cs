// Паттерн Прототип (Prototype)

// Интерфейс прототипа устройства
public interface IDevicePrototype
{
    IDevicePrototype Clone();
    void SetState(IDeviceState state);
}

// Базовый класс устройства
public abstract class Device : IDevicePrototype
{

    public Device(Device d){
        Name = d.Name;
        Type = d.Type;
        currentState = d.currentState;
        interface = d.interface;
    }

    public Device(IDeviceState state, IInternetInterface internet){
        currentState = state;
        interface = internet;
    }


    public string Name { get; set; }
    public string Type { get; set; }
    protected IDeviceState currentState;
    protected IInternetInterface interface;

    public abstract IDevicePrototype Clone();
    public abstract void SetState(IDeviceState state);

    public void TurnOn()
    {
        currentState.TurnOn(this);
    }

    public void TurnOff()
    {
        currentState.TurnOff(this);
    }
}

// Конкретный прототип устройства - лампа
public class Lamp : Device
{
    public Lamp()
    {
        currentState = new OffState();
    }

    public Lamp(Lamp L): base(L)
    {
        
    }

    public override IDevicePrototype Clone()
    {
        return (IDevicePrototype) new Lamp(this);
    }

    public override void SetState(IDeviceState state)
    {
        currentState = state;
    }
}

// Паттерн Мост (Bridge)

// Интерфейс реализации устройства
public interface IInternetInterface
{
    void ConnectToInternet();
    void DisconnectFromInternet();
}

// Конкретная реализация устройства - лампа
public class WIFI : IInternetInterface
{
    private bool isConnectedToInternet;

    public void ConnectToInternet()
    {
        isConnectedToInternet = true;
        Console.WriteLine("Device is connected to the internet by WI-FI.");
    }

    public void DisconnectFromInternet()
    {
        isConnectedToInternet = false;
        Console.WriteLine("Device is disconnected from the internet.");
    }
}

public class Bluetooth : IInternetInterface
{
    private bool isConnectedToInternet;

    public void ConnectToInternet()
    {
        isConnectedToInternet = true;
        Console.WriteLine("Device is connected to the internet by bluetooth.");
    }

    public void DisconnectFromInternet()
    {
        isConnectedToInternet = false;
        Console.WriteLine("Device is disconnected from the internet.");
    }
}

// Паттерн Состояние (State)

// Интерфейс состояния устройства
public interface IDeviceState
{
    void HandlePowerButton(Device device);
}

// Конкретное состояние - устройство включено
public class OnState : IDeviceState
{
    public void HandlePowerButton(Device device)
    {
        Console.WriteLine($"Device {device.Name} is already turned on.");
    }
}

// Конкретное состояние - устройство выключено
public class OffState : IDeviceState
{
    public void HandlePowerButton(Device device)
    {
        Console.WriteLine($"Turning on device {device.Name}.");
        device.SetState(new OnState());
        device.ConnectToInternet();
    }
}

// Пример использования всех паттернов

// Создаем оригинальное устройство (прототип)
Lamp originalLamp = new Lamp(new OffState(), new WIFI());
originalLamp.Name = "Living Room Lamp";
originalLamp.Type = "Smart Lamp" ;

// Клонируем оригинальное устройство
Lamp clonedLamp = (Lamp)originalLamp.Clone();
clonedLamp.Name = "Bedroom Lamp";

// Создаем абстракцию устройства, связываем ее с реализацией
DeviceAbstraction lampAbstraction = new DeviceAbstraction(clonedLamp, WIFI);

// Включаем и выключаем лампу
lampAbstraction.TurnOn();
lampAbstraction.TurnOff();